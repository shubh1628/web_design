let currentInput = '';

function appendToDisplay(value) {
  currentInput += value;
  document.getElementById('display').value = currentInput;
}

function clearDisplay() {
  currentInput = '';
  document.getElementById('display').value = currentInput;
  document.getElementById('output').value = '';
}

function calculateResult() {
  try {
    const result = eval(currentInput);
    document.getElementById('output').value = result;
  } catch (error) {
    document.getElementById('output').value = 'Error';
  }
}
