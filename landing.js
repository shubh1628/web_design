
document.addEventListener("DOMContentLoaded", function () {
    
    const emailForm = document.getElementById("email-form");

    
    emailForm.addEventListener("submit", function (e) {
        e.preventDefault(); 

        // Get the user's email address
        const email = document.getElementById("email").value;

        // we can add code here to send the email to your server for processing
        // For now, we'll just display a message
        alert(`Thank you for signing up with email: ${email}`);

        // Reset the form (clear the email input)
        emailForm.reset();
    });
});
